GW-Systems Parent POM
=================
The parent Maven POM for GW-Systems OSS projects.

What is it?
-----------
The GW-Systems parent POM provides default configuration for Maven builds.

* Recommended/Default versions for the most commonly used Maven plugins
* Manifest configuration for the jar and assembly plugins
* Profiles for generating source jars, and enforcing a minimum versions of
  Java and Maven
* Distribution Management and other configuration for deploying to the
  JBoss.org Maven repositories

How to use it?
--------------
Start out by adding the parent configuration to your pom.

    <parent>
      <groupId>com.gw-systems</groupId>
      <artifactId>parent</artifactId>
      <version>1.0</version>
    </parent>

The pom includes properties which allow various build configuration to be
customized.  For example, to override the default version of the
maven-compiler-plugin, just set a property.

    <properties>
      <version.compiler.plugin>3.1</version.compiler.plugin>
    </properties>

Or override the default Java compiler source and target level used in the build.
Note the default level is 1.7.

    <properties>
      <maven.compiler.target>1.6</maven.compiler.target>
      <maven.compiler.source>1.6</maven.compiler.source>
    </properties>

The minimum version of Java or Maven required to run a build can also be set via
properties.

    <properties>
      <maven.min.version>3.0.3</maven.min.version>
      <jdk.min.version>1.7</jdk.min.version>
    </properties>

If jdk.min.version is not set, it default to version defined by maven.compiler.source

For the full list of properties, refer to the POM itself.


The GW-Systems Release Profile
--------------------
The parent POM includes a Maven profile called "gw-release".  This profile contains
settings for generating a full project source archive, javadoc jar files, and
release deployment metadata.  If using the Maven release plugin, this profile
will automatically be activate during the release:perform step.

If the Maven release plugin is not used during the release process, the profile
can be manually activated from the command line during a release build.

    mvn -Pgw-release deploy


The GPG Sign Profile
--------------------
This POM includes a Maven profile called "gpg-sign" which provides default
configuration to generate GPG signatures for the build artifacts.

    mvn -Pgpg-sign deploy

In order for the gpg plugin to properly create a signature for each artifact,
the properties "gpg.keyname" and "gpg.passphrase" must be available to
the current build.  These properties can either be set in a
build profile, or on the command line.

    <profile>
      <id>gpg-config</id>
      <properties>
        <gpg.keyname>me@home.com</gpg.keyname>
        <!-- Don't keep passphrase in plain text! -->
        <gpg.passphrase>secret</gpg.passphrase>
      </properties>
    </profile>


Where to get more information?
---------------------------------
The [bitbucket wiki](https://bitbucket.org/paalgyula/gw-systems-parent-pom/wiki) provides some
additional examples.

License
-------
* This software is in the public domain